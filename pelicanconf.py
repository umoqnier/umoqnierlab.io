#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Diego A. Barriga'
SITENAME = 'Memorias de un Miope'
SITESUBTITLE = 'Siempre empujar las fronteras del conocimiento'
SITEURL = 'https://umoqnier.gitlab.io'
TIMEZONE = 'America/Mexico_City'

THEME = 'themes/flasky'

PLUGIN_PATHS = ["plugins/"]

DEFAULT_CATEGORY = 'Sin categoría'
DATE_FORMAT = {
    'es': '%d/%m/%Y'
}

SECTIONS = (('Blog', 'index.html'), ('Acerca del miope',
                                             'pages/acerca-de-mi.html'))

DEFAULT_DATE_FORMAT = '%d/%m/%Y'
DEFAULT_PAGINATION = 10
PDF_GENERATOR = True
REVERSE_CATEGORY_ORDER = True

PATH = 'content'
OUTPUT_PATH = 'public'

DEFAULT_LANG = 'es'

FEED_RSS = 'feeds/all.rss.xml'
CATEGORY_FEED_RSS = 'feeds/{slug}.rss.xml'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# static paths will be copied under the same name
STATIC_PATHS = ["images"]

# Optional social media links
TWITTER_USERNAME = 'umoqnier'
GITHUB_URL = 'http://github.com/umoqnier/'
MAIL_USERNAME = 'diegobarriga'
MAIL_HOST = 'protonmail.com'

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True

DESCRIPTION = "Este blog es escrito por un miope que quiere escribir más de 120 caractéres :)"
