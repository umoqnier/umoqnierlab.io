---
Title: Calabozos, Magia y personajes
Date: 2020-08-13
Modified: 2020-08-17
Category: Personal
Tags: board-games, amigxs, dnd, magic, rol
Slug: calabozos-magia
---

Esta noche fue la primera vez que juego con mis colegas a Calabozos y Dragones
(*Dungeons and Dragons*) desde el encierro. Esta partida está ambientada en el
mundo de **Ravnica** (un universo perteneciente a otro popular juego de cartas
llamado *Magic The Gathering*). Este universo está conformado por varios gremios
y cada uno tiene sus propios rasgos de personalidad, objetivos y visión del
mundo.

Como es una de las sesiones iniciales fue dedicada enteramente a la creación de
personajes y debo decir que fue muy divertido aunque haya tomado más tiempo del
que pensé (más de un par de horas). Gracias a mis colegas Óscar, Emiliano (que
fueron quienes dirigieron la sesión), Vicky, Carla y Aura el proceso fue más
amigable.

Yo he decidido ser un **Mago Izzet** llamado *Salvor Astaroth*. Este mago es
distraído, convencido de que la aplicación de principios lógicos nos conducirá
hacia el progreso, que hará todo lo que su *guild master* (un dragón ancestral
bastante mal humorado pero al parecer sumamente inteligente) ordene y olvidará
(o si no ignorará) cualquier plan que le hayan puesto enfrente. Es interesante
dado que estos rasgos de personalidad son generados aleatoriamente con base en
tiradas de diversos dados.

![Mago izzet](images/magician.jpg){width=50% class="align-center"}

Aunque aun debo darle cierto trasfondo a mi personaje me agrada pertenecer a la
facción de científicos (o como se les conoce en Internet: 100tifikos) del
universo de **Ravnica**. Creo que en cierto sentido empata con lo que creo
respecto del progreso pero de una forma más caricaturesca.

Las sesiones venideras prometen diversión en cualquier caso. Espero poder
aprenderme ese compendio de hechizos que tanto miedo me da pero que se que me
será útil. No quiero olvidar como lanzar esa bola de fuego en el momento crucial
de mi aventura :D
