---
Title: Fuimos a Teotihuacán
Date: 2020-09-12
Category: Viaje
Tags: amigxs, bicicleta, turismo
Slug: viaje-teotihuacan
summary: Fui a teotihuacan en bicicleta y me chingue la rodilla
---

Hoy he tenido la oportunidad de, por fin, ir a las pirámides de Teotihuacán.
Esto después de ser la burla de muchos camaradas por no haber ido en mis primeros
años escolares (por las reacciones me parece que es un rito casi obligado) y lo
mejor de todo es que fui en **Bicicleta**.

![Mi espalda](/images/yo-bici.jpg "Mis amigos y yo llendo en bicicleta hacia
Teotihuacán"){width="80%" class="align-center"}

Tuve la suerte de disfrutar de la compañía de varios amigos que me auxiliaron
para no morir en el intento. Cabe aclarar que no me consideraría un hábil
bicicletero pero bicicletero soy al final y termine el viaje. Fue una ruta
divertida, cansada, intensa y memorable. Sin duda lo volvería a hacer.

Con esta, sería la segunda vez que hago un viaje "largo" y lo he notado ya que a
causado estragos. En particular, en mi rodilla que ya para la última parte de
regreso estaba pidiendo clemencia :p. Para mi suerte mis camaradas (pobres) me
empujaron un tramo y logré llegar a casa (casi) completo.

Lamentablemente por la plaga no pudimos subir las pirámides así que tengo la
obligación de volver por la revancha. Me asombra lo gigantesco que es el lugar y
me pregunto cuales habrán sido las dinámicas de esos tiempos en cuanto la vida
diaria.

Prefiero ahorrar ancho de banda omitiendo mis terribles fotografías. Ya
hay muchas y muy lindas en
[Wikipedia](https://es.wikipedia.org/wiki/Teotihuac%C3%A1n) pero me permito
presentarles a un amigo que hicimos porallá :)

![Lomito Suavecito](/images/lomito.jpg "Un perrito bonito"){width="50%" class="align-center"}

Es la primera vez que escribo algo sobre mi experiencia andando en bicicleta y
me gustaría dejar unas pocas reflexiones al respecto.

1. Creo que ha sido una excelente decisión adquirir una
   bicicleta y una mejor decisión usarla como mi medio de transporte primario.
   Estoy convencido de que este viaje ha sido más divertido en gran medida
   porque mis piernas fueron las que me llevaron (bueno, también mis amix).
2. Es un hecho que la Ciudad Monstruo (mejor conocida como exDF) es peligrosa
   para rodar. Reconozco también que mi pueblo Mordor (mejor conocido como Edo.
   Mex.) es menos amigable para lxs ciclistas pero con mucho cuidado y un poco
   de suerte puedes tener una experiencia agradable que le desearía incluso a mi
   peor enemigo.
3. Me recuerdo a mi mismo pensando: "¿Cómo voy a llegar hasta la CDMX desde el
   Estado de México?". Pues he llegado a Teotihuacán y ha sido difícil pero
   posible. Esta bien tener miedo pero hay que ser valientes :D
4. Si tu estas pensando en comprar un bicicleta **¡hazlo!**. No te arrepentirás,
   hay muchos colectivos con quienes rodar, encontrar rutas y acompañarse. Si no
   encuentras con quien puedes unirte a nuestro club de bicicletos solo
   escribeme 💙

![Yo con mi bici](images/yo-bici-2.jpg "Yo mero con mi
bicicleta"){height="400" class="align-center"}
