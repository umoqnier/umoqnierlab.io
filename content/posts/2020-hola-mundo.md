---
Title: Hola Mundo Cruel y Despiadado
Date: 2020-08-12
Category: Personal
Tags: blog, covid19
Slug: hola-mundo-cruel
---

Hola mundo cruel y despiadado. Te saluda un joven desde el encierro causado por
la plaga del Coronavirus `SARS-CoV2`. En todo caso seré breve dado que
ya voy tarde para el seminario de LISP del que soy participe y debo dejar
este blog corriendo para antes de que sea la hora de mi partida.

Cuando tenga bien afianzado todo el proceso para montar un blog de este estilo
planeo hacer unos tutoriales y quizá hasta un curso en línea, que por supuesto
están de moda, para que alguien más pueda utilizarlo. Con algo mas que
suerte quizá retorne la dorada época en la que la gente solía escribir en blogs.

Es preciso decir que me cuelgo de la plataforma llamada
[Gitlab](https://gitlab.com/) para no pagar un peso por el hospedaje de está
página y pretendo que mucha gente se aproveche de está característica que
puede ser útil.

Me extendí más de lo planeado y ya vamos tarde. Ahí nos vidrios :p
