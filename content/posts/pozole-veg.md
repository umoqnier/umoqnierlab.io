---
Title: Pozole vegetariano
Date: 2020-11-29
Category: Recetas
Tags: caliente, caldo, intermedio
Slug: recetas/pozole-veg
Summary: Pozole rojo pero en lugar de pollo tiene champiñones y flor de calabaza :d
---

## Ingredientes

1. Dos bolsas de granos de maíz pozolero - 800 gr
2. Elotes - 4 o 5
3. Chiles guajillos - 8
    * Puede ser del que pica o del que no pica
4. Champiñones - ½ Kg
5. Un manojo de flor de calabaza
6. Un pedazo de cebolla - ¼
7. Una cabeza de ajo
8. Sal y pimienta

## Preparación

1. Hervir granos de pozole con una cabeza de ajo mediana
2. Hervir el elote desgranados
3. Cuando estén tiernos juntarlos. Con el agua de ambos se hace el pozole
4. Desvenar los chiles y hervir
5. Moler los chiles con un pedazo de cebolla y después poner en el caldo de maíz + elote
6. Incorporar los champiñones y flor de calabaza
7. Sazonador al gusto (sal, pimienta o saboreador)
