---
Title: Hola, soy Diego 🤖
Date: 2020-08-12
Category: Personal
Tags: s
Slug: acerca-de-mi
---

Me llamo Diego Barriga, tengo miopía y esto es lo que hago para ganarme la
chuleta (vegetariana por supuesto):

* Hago mi tesis sobre NLP para lenguas mexicanas 🔬
* Desarrollo software en [Comunidad
Elotl](https://elotl.mx/) 🌽
* Soy parte de [Laboratorio de Investigación y Desarrollo de Software
  Libre](https://lidsol.org/) 📡
* Hago danza aérea, soy músico amateur 🎝 y me muevo en bicicleta 🚲

## Intereses

* Natural Language Processing (*NLP*)
    * *Low resources*
* Machine Learning (*ML*)
* Cultura libre
    * Privacidad y anonimato
    * Licencias libres
    * Comunidades
* Ciencia ficción
* Música
